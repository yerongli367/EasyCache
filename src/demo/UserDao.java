package demo;

import com.whiteblue.cache.ClearCache;
import com.whiteblue.cache.UseCache;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by WhiteBlue on 15/5/18.
 */
public class UserDao {

    @UseCache(cacheName = "user")
    public User getUser() {
        return new User();
    }

    @ClearCache(cacheName = "users")
    public List<User> getUsers() {
        return new ArrayList<User>();
    }

    @ClearCache(cacheName = {"user","users"})
    public void deleteUser() {
    }
}
