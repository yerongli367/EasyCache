#EasyCache


<h2>致力于让缓存使用变得简单轻松(误..)</h2>
<p>依赖:ehcache.jar</p>

<h3>使用缓存:</h3>

<pre>

 @UseCache(cacheName = "user")
    public User getUser() {
        return new User();
    }

</pre>

<p>会根据方法的参数生成key,cache的name自行在配置文件里写</p>

<h3>清除缓存:</h3>

<pre>
    //单个
    @ClearCache(cacheName = "users")
    public List<User> getUsers() {
        return new ArrayList<User>();
    }

    //多个
    @ClearCache(cacheName = {"user","users"})
    public void deleteUser() {
    }

</pre>

<h3>取得代理对象:</h3>


<pre>

    UserDao dao = CacheFactory.get(UserDao.class);
    dao.getUser();
    dao.getUsers();
    dao.deleteUser();

</pre>

<p>以上，之后会缓慢更新(如果有想法的话)</p>